<html xmlns:py="http://genshi.edgewall.org/" xmlns:xi="http://www.w3.org/2001/XInclude" py:strip="">

<p>
Git repositories are located at
<code>git://github.com/phpmyadmin/REPONAME.git</code> and you
can browse them online using <a
href="http://github.com/phpmyadmin/">github</a>.
</p>

</html>
